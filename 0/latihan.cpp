#include <iostream>
using namespace std;

int main ()
{
	cout << "Program input nilai mahasiswa" << endl;
	cout << "================================================" << endl;
	cout << "Masukkan jumlah mahasiswa: ";
	int n;
	cin >> n;
	string mahasiswa[n];
	int nilai[n];
	for (int i = 0; i < n; i++)
	{
		cout << "Masukkan nama mahasiswa ke-" << i + 1 << ": ";
		cin >> mahasiswa[i];
	}
	for (int i = 0; i < n; i++)
	{
		cout << "Masukkan nilai mahasiswa ke-" << i + 1 << ": ";
		cin >> nilai[i];
	}

	cout << "================================================" << endl;
	// menampilkan nilai mahasiswa
	cout << "Nilai mahasiswa: ";
	for (int i = 0; i < n; i++)
	{
		cout << mahasiswa[i] << " " << nilai[i] << " ";
		cout << endl;
	}
	
	return 0;
}
